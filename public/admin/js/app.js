
var adminApp = angular.module('mean-blog.admin', [
	'ui.router',
	'btford.markdown',
	'mean-blog.posts'
]);

adminApp.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/');
	
	$stateProvider
		.state('allPosts', {
			url: '/',
			templateUrl: '/admin/templates/allPosts.html',
			resolve: {
				postList: function(Posts){
					return Posts.all().then(function(data){
						return data;
					});
				}
			},
			controller: 'AllPostsCtrl'
		})
		.state('myDraft', {
			url: '/',
			templateUrl: '/admin/templates/allPostsDraft.html',
			resolve: {
				DraftPosts: function(DraftPosts){
					return DraftPosts.all().then(function(data){
						return data;
					});
				}
			},
			controller: 'AllPostsDraftCtrl'
		})
		.state('addPost', {
			url: '/addPost',
			templateUrl: '/admin/templates/addPost.html',
			controller: 'AddPostCtrl'
		})
		.state('editPost', {
			url: '/editPost/{id}',
			templateUrl: '/admin/templates/addPost.html',
			resolve: {
				Posts: function(Posts){
					return Posts.all().then(function(data){
						return data;
					});
				}
			},
			controller: 'editPostCtrl'
		});
});

