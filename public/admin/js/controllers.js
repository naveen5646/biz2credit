
adminApp.controller('NavCtrl', function($scope, $state){
	$scope.active = $state;
	$scope.isActive = function(viewLocation){
		var active = (viewLocation === $state.current.name);
		return active;
	};
});

adminApp.controller('AllPostsCtrl', function($scope, postList){
	$scope.posts = postList;
	$scope.activePost = false;
	$scope.setActive = function(post){
		$scope.activePost = post;
	}
});
adminApp.controller('AllPostsDraftCtrl', function($scope, DraftPosts){
	$scope.posts = DraftPosts;
	$scope.activePost = false;
	$scope.setActive = function(post){
		$scope.activePost = post;
	}
});


adminApp.controller('editPostCtrl', function($scope, Post,$stateParams){
	$scope.id = $stateParams.id;
	$scope.Post = Post;
	$scope.activePost = false;
	$scope.setActive = function(post){
		$scope.activePost = post;
	}
});


adminApp.controller('AddPostCtrl', function($scope, Posts){
	$scope.post = {};
	$scope.addPost = function(newPost){
		newPost.status = 1;
		Posts.add(newPost).then(function(res){
			console.log(res);
		});
	};
	$scope.addDraft = function(newPost){
		newPost.status = 2;
		console.log(newPost);
		Posts.add(newPost).then(function(res){
			console.log(res);
		});
	};


});